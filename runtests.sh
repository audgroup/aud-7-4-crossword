#!/bin/bash

rm -r ./out/
mkdir ./out/
cp src/*.txt ./out/
javac -d ./out/ src/*.java

cd out
java CrosswordPuzzleExamples > actual.txt

diff -u CrosswordPuzzleExamplesOutput.txt actual.txt
if [ $? -ne 0 ]; then
	echo "Diff failure" >&2
	exit 1
fi

time java CrosswordPuzzleExamples > /dev/null
