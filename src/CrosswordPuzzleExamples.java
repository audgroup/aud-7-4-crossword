public class CrosswordPuzzleExamples extends CrosswordPuzzleSolver {
	public static void main(String[] args) {
		System.out.println("--- SMOKY --- SMOKY --- SMOKY --- SMOKY --- SMOKY --- SMOKY --- SMOKY --- SMOKY ---");
		mainTestSmoky();
		System.out.println("\n\n.oO BABY Oo.oO BABY Oo.oO BABY Oo.oO BABY Oo.oO BABY Oo.oO BABY Oo.oO BABY Oo.oO BABY Oo.");
		mainTestBaby();
		System.out.println("\n\n### EASY ### EASY ### EASY ### EASY ### EASY ### EASY ### EASY ### EASY ### EASY ###");
		mainTestEasy();
	}

	private static final void mainTestSmoky() {
		// (y-coordinate, x-coordinate, length, direction: 0=horizontal, 1=vertical)
		int[][] gridSpec = { { 0, 0, 10, 1 }, { 0, 0, 8, 0 }, { 0, 4, 10, 1 }, { 0, 7, 4, 1 }, { 0, 9, 10, 1 }, { 0, 9, 9, 0 }, { 0, 12, 4, 1 }, { 0, 15, 6, 1 }, { 0, 17, 10, 1 }, { 2, 0, 5, 0 }, { 3, 4, 4, 0 }, { 3, 6, 7, 1 }, { 3, 9, 5, 0 }, { 3, 11, 7, 1 }, { 4, 0, 5, 0 }, { 5, 6, 4, 0 }, { 5, 11, 7, 0 }, { 6, 0, 5, 0 }, { 6, 2, 4, 1 }, { 7, 4, 4, 0 }, { 7, 9, 9, 0 }, { 9, 0, 5, 0 }, { 9, 6, 4, 0 }, { 9, 11, 7, 0 } };
		String[] wordsSpec = { "INGE", "NOAH", "ORBI", "ROBE", "ROLL", "SEIL", "STAR", "DOLDE", "HAUPT", "INSEL", "KOPIE", "MASSE", "MOEBEL", "GASOLIN", "INSULIN", "PRIMAER", "RAUCHER", "SIGNATUR", "EIFOERMIG", "STAMMGAST", "ARTILLERIE", "ELEKTRISCH", "GESPENSTER", "SCHWIMMBAD" };

		// check riddle
		int evaluateSpec = evaluateSpec(gridSpec, wordsSpec);
		System.out.println("# evaluateSpec: " + evaluateSpec);
		System.out.println("========================================");
		if (0 == evaluateSpec) {
			boolean[] placementV, placementH;
			// create grid
			System.out.println("# generate empty grid");
			char[][] grid = generateGrid(gridSpec);
			printGrid(grid, true);
			System.out.println("========================================");
			// place/unplace V-H
			System.out.println("# set word \"" + wordsSpec[wordsSpec.length - 3] + "\"");
			placementV = setWord(gridSpec[4], wordsSpec[wordsSpec.length - 3], grid);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# is word valid \"" + wordsSpec[6] + "\": " + isWordValid(gridSpec[15], wordsSpec[6], grid));
			System.out.println("========================================");
			System.out.println("# set word \"" + wordsSpec[6] + "\"");
			placementH = setWord(gridSpec[15], wordsSpec[6], grid);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# remove word \"" + wordsSpec[6] + "\"");
			removeWord(gridSpec[15], wordsSpec[6], grid, placementH);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# remove word \"" + wordsSpec[wordsSpec.length - 3] + "\"");
			removeWord(gridSpec[4], wordsSpec[wordsSpec.length - 3], grid, placementV);
			printGrid(grid, true);
			System.out.println("========================================");
			// place/unplace H-V
			System.out.println("# set word \"" + wordsSpec[6] + "\"");
			placementH = setWord(gridSpec[15], wordsSpec[6], grid);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# is word valid \"" + wordsSpec[wordsSpec.length - 3] + "\": " + isWordValid(gridSpec[4], wordsSpec[wordsSpec.length - 3], grid));
			System.out.println("========================================");
			System.out.println("# set word \"" + wordsSpec[wordsSpec.length - 3] + "\"");
			placementV = setWord(gridSpec[4], wordsSpec[wordsSpec.length - 3], grid);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# remove word \"" + wordsSpec[wordsSpec.length - 3] + "\"");
			removeWord(gridSpec[4], wordsSpec[wordsSpec.length - 3], grid, placementV);
			printGrid(grid, true);
			System.out.println("========================================");
			System.out.println("# remove word \"" + wordsSpec[6] + "\"");
			removeWord(gridSpec[15], wordsSpec[6], grid, placementH);
			printGrid(grid, true);
			System.out.println("========================================");
		}
		// change slightly and than re-check riddle
		wordsSpec[0] = "JOHNDOE";
		System.out.println("# evaluateSpec: " + evaluateSpec(gridSpec, wordsSpec)); // => code 8
		System.out.println("========================================");
	}

	private static final void mainTestBaby() {
		int[][] gridSpec = { { 0, 0, 10, 1 }, { 0, 0, 8, 0 }, { 0, 4, 10, 1 }, { 0, 7, 4, 1 }, { 0, 9, 10, 1 }, { 0, 9, 9, 0 }, { 0, 12, 4, 1 }, { 0, 15, 6, 1 }, { 0, 17, 10, 1 }, { 2, 0, 5, 0 }, { 3, 4, 4, 0 }, { 3, 6, 7, 1 }, { 3, 9, 5, 0 }, { 3, 11, 7, 1 }, { 4, 0, 5, 0 }, { 5, 6, 4, 0 }, { 5, 11, 7, 0 }, { 6, 0, 5, 0 }, { 6, 2, 4, 1 }, { 7, 4, 4, 0 }, { 7, 9, 9, 0 }, { 9, 0, 5, 0 }, { 9, 6, 4, 0 }, { 9, 11, 7, 0 } };
		String[] wordsSpec = { "INGE", "NOAH", "ORBI", "ROBE", "ROLL", "SEIL", "STAR", "DOLDE", "HAUPT", "INSEL", "KOPIE", "MASSE", "MOEBEL", "GASOLIN", "INSULIN", "PRIMAER", "RAUCHER", "SIGNATUR", "EIFOERMIG", "STAMMGAST", "ARTILLERIE", "ELEKTRISCH", "GESPENSTER", "SCHWIMMBAD" };
		if (0 == evaluateSpec(gridSpec, wordsSpec)) {
			printGrid(solvePuzzle(gridSpec, wordsSpec), false);
		}
	}

	private static final void mainTestEasy() {
		int[][] gridSpec = { { 14, 0, 9, 0 }, { 14, 11, 8, 0 }, { 12, 10, 9, 0 }, { 11, 0, 8, 0 }, { 10, 7, 8, 0 }, { 9, 0, 6, 1 }, { 9, 0, 8, 0 }, { 9, 7, 6, 1 }, { 8, 10, 9, 0 }, { 8, 14, 7, 1 }, { 7, 0, 9, 0 }, { 7, 4, 8, 1 }, { 7, 16, 6, 1 }, { 7, 18, 8, 1 }, { 6, 8, 6, 0 }, { 6, 11, 9, 1 }, { 5, 0, 7, 0 }, { 3, 6, 6, 0 }, { 3, 13, 6, 0 }, { 2, 0, 7, 0 }, { 2, 6, 6, 1 }, { 0, 0, 8, 1 }, { 0, 0, 9, 0 }, { 0, 3, 8, 1 }, { 0, 8, 8, 1 }, { 0, 10, 7, 1 }, { 0, 10, 9, 0 }, { 0, 13, 7, 1 }, { 0, 15, 6, 1 }, { 0, 18, 6, 1 } };
		String[] wordsSpec = { "TOURISMUS", "RESIDENT", "STRAFE", "ATTACKE", "REISEWEG", "POSTKARTE", "BEISSER", "KONSERVE", "REGION", "PLAPPERER", "KRIEGEN", "PHRASE", "NUSSTORTE", "LAUSBUB", "PALETTE", "ORDNER", "TRAGZEIT", "ERNTEMOND", "HOFRAT", "ERZENGEL", "BEWACHUNG", "TELESKOP", "BANNER", "ABLENKUNG", "GALERIST", "GEAEST", "ANLASS", "SPRINGER", "ABREDE", "SCHLEPPE" };
		if (0 == evaluateSpec(gridSpec, wordsSpec)) {
			printGrid(solvePuzzle(gridSpec, wordsSpec), false);
		}
	}

	private static final void printGrid(char[][] grid, boolean gridFill) {
		if (grid != null && grid[0] != null) {
			for (int i = grid[0].length; i >= 0; i--) {
				System.out.print(i != 0 ? "--" : "---\n");
			}
			for (char[] row : grid) {
				System.out.print("| ");
				if (row != null) {
					for (char field : row) {
						System.out.print((field != 0 ? field : gridFill ? "." : " ") + " ");
					}
				}
				System.out.println("|");
			}
			for (int i = grid[0].length; i >= 0; i--) {
				System.out.print(i != 0 ? "--" : "---\n");
			}
		}
	}
}
