public class CrosswordPuzzleSolver {
	public static int evaluateSpec(int[][] gridSpec, String[] wordsSpec) {
		// Constants for magic numbers
		final int Y = 0;
		final int X = 1;
		final int LENGTH = 2;
		final int DIRECTION = 3;
		final int ACROSS = 0;
		final int DOWN = 1;

		// Grid spec has at least one field
		if (gridSpec == null || gridSpec.length < 1)
			return 1;

		// Amount of words matches amount of fields
		if (wordsSpec == null || gridSpec.length != wordsSpec.length)
			return 2;

		// Check if grid spec entries are in the valid range
		// (x >= 0 && y >= 0)
		for (int[] gridSpecEntry : gridSpec) {

			if (gridSpecEntry[X] < 0 || gridSpecEntry[Y] < 0)
				return 3;

		}

		// Every word field has a length of at least one letter
		for (int[] gridSpecEntry : gridSpec) {

			if (gridSpecEntry[LENGTH] < 1)
				return 4;

		}

		// Every word field has a valid direction
		for (int[] gridSpecEntry : gridSpec) {
			if (gridSpecEntry[DIRECTION] != DOWN && gridSpecEntry[DIRECTION] != ACROSS)
				return 5;
		}

		// Check that word fields don't overlap each other in the same dimension
		// Of course, they're allowed to cross each other

		// Not entirely certain if I can create a new class in this Uebung, so nasty array with magic
		// numbers is unavoidable :(
		final int WIDTH = 0;
		final int HEIGHT = 1;
		int[] dimensions = getGridDimensions(gridSpec);

		// Undo magic numbers
		int y_max = dimensions[HEIGHT];
		int x_max = dimensions[WIDTH];

		// Now create a test matrix where every 'taken' place in that dimension is marked with an 'x'
		// Initialize test matrices
		char[][] yTest = new char[y_max][x_max];
		char[][] xTest = new char[y_max][x_max];


		for (int[] gridSpecEntry : gridSpec) {
			int y = gridSpecEntry[Y];
			int x = gridSpecEntry[X];
			int length = gridSpecEntry[LENGTH];
			int direction = gridSpecEntry[DIRECTION];

			if (direction == ACROSS) {
				// Check horizontal direction
				for (int j = x; j < (x + length); j++) {
					if (xTest[y][j] == 'x')
						return 6;
					xTest[y][j] = 'x';
				}
			} else {
				// Check vertical direction
				for (int j = y; j < (y + length); j++) {
					if (yTest[j][x] == 'x')
						return 6;
					yTest[j][x] = 'x';
				}
			}
		}

		// Each word in wordSpec should have at least one letter
		for (String word : wordsSpec) {
			if (word.length() < 1)
				return 7;
		}

		// The available fields' lengths match the word lengths

		// To make a frequencies array, we need to establish the domain
		// What's the longest word?
		int maxLength = 0;
		for (int[] gridSpecEntry : gridSpec) {
			if (gridSpecEntry[LENGTH] > maxLength)
				maxLength = gridSpecEntry[LENGTH];
		}

		int[] gridSpecLengthFrequencies = new int[maxLength + 1];
		int[] wordLengthFrequencies = new int[maxLength + 1];


		for (int[] gridSpecEntry : gridSpec)
			gridSpecLengthFrequencies[gridSpecEntry[LENGTH]]++;

		for (String word : wordsSpec)
			wordLengthFrequencies[word.length()] += 1;

		for (int i = 0; i < maxLength; i++) {
			if (gridSpecLengthFrequencies[i] != wordLengthFrequencies[i])
				return 8;
		}

		// If none of the error-codes got thrown: perfect =)
		return 0;
	}

	private static int[] getGridDimensions(int[][] gridSpec) {
		// Constants to get around magic numbers
		final int WIDTH = 0;
		final int HEIGHT = 1;

		final int Y = 0; // Who the fuck does Y first?!?
		final int X = 1;
		final int LENGTH = 2;
		final int DIRECTION = 3;
		final int ACROSS = 0;

		// Now let's do the actual work
		int[] dimensions = new int[2];

		for (int[] GridSpecElement : gridSpec) {
			if (GridSpecElement[DIRECTION] != ACROSS) {
				if ((GridSpecElement[Y] + GridSpecElement[LENGTH]) > dimensions[HEIGHT])
					dimensions[HEIGHT] = GridSpecElement[Y] + GridSpecElement[LENGTH];
			} else {
				if ((GridSpecElement[X] + GridSpecElement[LENGTH]) > dimensions[WIDTH])
					dimensions[WIDTH] = (GridSpecElement[X] + GridSpecElement[LENGTH]);
			}
		}
		return dimensions;
	}

	public static char[][] generateGrid(int[][] gridSpec) {
		final int WIDTH = 0;
		final int HEIGHT = 1;

		final int[] dimensions = getGridDimensions(gridSpec);

		final int gridWidth = dimensions[WIDTH];
		final int gridHeight = dimensions[HEIGHT];

		final char UNUSED_FIELD = 0;

		return fill2DArray(new char[gridHeight][gridWidth], UNUSED_FIELD);
	}

	private static char[][] fill2DArray(char[][] array, char character) {
		for (int i = 0; i < array.length; i++)
			for (int j = 0; j < array[i].length; j++)
				array[i][j] = character;

		return array;
	}


	public static boolean isWordValid(int[] gridSpecEntry, String wordsSpecEntry, char[][] grid) {
		final int y = gridSpecEntry[0];
		final int x = gridSpecEntry[1];
		final int length = gridSpecEntry[2];
		final int direction = gridSpecEntry[3];

		//Compare the lenght of the word and the grid

		if (length != wordsSpecEntry.length())
			return false;

		//Compare the grid entries with the word

		if (direction == 0) {                         // horizontal Grids
			for (int i = x; i < (x + length); i++) {
				if (grid[y][i] != 0 && grid[y][i] != wordsSpecEntry.charAt(i-x))
					return false;
			}
		} else {                                        // Vertical Grids
			for (int j = y; j < (y + length); j++) {
				if (grid[j][x] != 0 && grid[j][x] != wordsSpecEntry.charAt(j-y))
					return false;
			}
		}
		return true;
	}

	public static boolean[] setWord(int[] gridSpecEntry, String wordsSpecEntry, char[][] grid) {
		// Magic number resolution
		final int Y = 0;
		final int X = 1;
		final int LENGTH = 2;
		final int DIRECTION = 3;
		final int ACROSS = 0;

		// Logic
		final int wordLength = wordsSpecEntry.length();

		boolean[] result = new boolean[wordLength];

		for (int i = 0; i < wordLength; i++) {
			if (gridSpecEntry[DIRECTION] == ACROSS) {
				// Booleans are false by default, so we only need to deal with them if we need
				// to make it true
				if (grid[gridSpecEntry[Y]][gridSpecEntry[X] + i] != wordsSpecEntry.charAt(i)) {
					grid[gridSpecEntry[Y]][gridSpecEntry[X] + i] = wordsSpecEntry.charAt(i);
					result[i] = true;
				}
			} else {
				if (grid[gridSpecEntry[Y] + i][gridSpecEntry[X]] != wordsSpecEntry.charAt(i)) {
					grid[gridSpecEntry[Y] + i][gridSpecEntry[X]] = wordsSpecEntry.charAt(i);
					result[i] = true;
				}
			}
		}

		return result;
	}

	/**
	 * Removes a word from the grid that was previously set with placeWord.
	 *
	 * @param gridSpecEntry
	 * @param wordsSpecEntry
	 * @param grid
	 * @param charsPlaced    Result array of method placeWord with the same gridSpecEntry, wordsSpecEntry, and grid parameters.
	 */
	public static void removeWord(int[] gridSpecEntry, String wordsSpecEntry, char[][] grid, boolean[] charsPlaced) {
		if (gridSpecEntry[3] == 0) {
			for (int dx = 0; dx < wordsSpecEntry.length(); dx++) {
				if (charsPlaced[dx]) {
					grid[gridSpecEntry[0]][gridSpecEntry[1] + dx] = 0;
				}
			}
		} else {
			for (int dy = 0; dy < wordsSpecEntry.length(); dy++) {
				if (charsPlaced[dy]) {
					grid[gridSpecEntry[0] + dy][gridSpecEntry[1]] = 0;
				}
			}
		}
	}

	public static char[][] solvePuzzle(int[][] gridSpec, String[] wordsSpec) {
		char[][] grid = generateGrid(gridSpec);

		// Call recursive helper function to solve the puzzle
		solve(gridSpec, grid, wordsSpec, 0, new boolean[gridSpec.length]);

		return grid;
	}

	private static boolean solve(final int[][] gridSpec, char[][] grid, final String[] wordsSpec, int currentField, boolean[] ignoreWords) {

		// We're iterating through the list of fields, if we hit the last field, we finished the puzzle
		if(currentField == gridSpec.length) {
			return true;
		}else{
			// Let's try to place a word in our current field
			for (int i = 0; i < wordsSpec.length; i++) {
				if(ignoreWords[i]) continue;

				int[] gridSpecEntry = gridSpec[currentField];
				String word = wordsSpec[i];
				if (isWordValid(gridSpecEntry, word, grid)){
					// Word fits, let's try what the puzzle looks like with the word here
					ignoreWords[i] = true;

					final boolean[] charsPlaced = setWord(gridSpecEntry, word, grid);

					// Recursively attempt to solve the rest of the puzzle with the word in this location
					if (solve(gridSpec, grid, wordsSpec, currentField + 1, ignoreWords)) {
						// We found a solution
						return true;
					} else {
						// No solution with this word here, remove it, and have the for-loop try the next
						// location for the word
						ignoreWords[i] = false;
						removeWord(gridSpecEntry, word, grid, charsPlaced);
					}

				}
			}

			// If we couldn't place the word in any grid spec, return that this path is dead
			return false;
		}
	}
}
